$ = jQuery
jQuery(document).ready(function ($) {

    /* header search input */
    $(".tab-detail .from a").click(function () {
        $(".search-from ").toggleClass("active");

    });

    $(".tab-detail .info.go-to a").click(function () {
        $(".search-go-to ").toggleClass("active");

    });

    $('.accordion-item .accordion-icon').click(function (e) {
        e.preventDefault();
        var $this = $(this);

        if ($this.siblings('.accordion-content').hasClass('show')) {
            $this.siblings('.accordion-content').removeClass('show');
            $this.siblings('.accordion-content').slideUp(350);
        } else {
            // $this.parent().parent().siblings('.accordion-item .accordion-content').removeClass('show');
            // $this.parent().parent().siblings('.accordion-item .accordion-content').slideUp(350);
            $this.parent('.accordion-item').siblings('.accordion-item').children('.accordion-content').removeClass('show');
            $this.parent('.accordion-item').siblings('.accordion-item').children('.accordion-content').slideUp(350);

            $this.siblings('.accordion-content').toggleClass('show');
            $this.siblings('.accordion-content').slideToggle(350);
        }
    });

    $('.accordion-item-wrap .accordion-item .accordion-icon').on('click', function (e) {
        e.preventDefault();
        $(this).parent().siblings('.accordion-item').removeClass('current');
        $(this).parent().toggleClass('current');

    });

    // acoordion
    $('.accordion-item .toggle').click(function (e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.next().hasClass('show')) {
            $this.next().removeClass('show');
            $this.next().slideUp(350);
        } else {
            $this.parent().parent().find('.accordion-item .accordion-content').removeClass('show');
            $this.parent().parent().find('.accordion-item .accordion-content').slideUp(350);
            $this.next().toggleClass('show');
            $this.next().slideToggle(350);
        }
    });
    $('.accordion-item-wrap .accordion-item .toggle').on('click', function (e) {
        e.preventDefault();
        $(this).parent().siblings('.accordion-item').removeClass('current');
        $(this).parent().toggleClass('current');

    });
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });


    $('#child-seat-btn').click( function(){
        $('.child-seat').toggleClass('active');
        if($('.child-seat').hasClass('active') ){
            $('.child-seat-request').html('Add a request');
        } else{
            $('.child-seat-request').html('Edit a request');
        }
        
    })

});