$ = jQuery
jQuery(document).ready(function ($) {

  jQuery('.menu-top-menu-container').meanmenu({
    meanMenuContainer: '.main-navigation',
    meanScreenWidth: "1023",
    meanRevealPosition: "right",
  });

  // slick slider on sign-in popup
  $('.slider-wrap').slick({
    infinite: true,
    autoplay: true,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
  });

  $('.tags-slider-wrap').slick({
    infinite: true,
    autoplay: true,
    dots: false,
    arrows: false,
    slidesToScroll: 1,
    speed: 300,
    slidesToShow: 1,
    variableWidth: true
  });

  $('.flights-filter-airways-slider-content').slick({
    infinite: false,
    autoplay: true,
    dots: false,
    arrows: true,
    slidesToShow: 6,
    slidesToScroll: 1,
  });

  $('.offer-flight-slider').slick({
    infinite: false,
    dots: false,
    arrows: false,
    slidesToShow: 2.5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1.5,
        }
      }
    ]
  });

  $('.offer-hotels-slider, .offer-cars-slider').slick({
    infinite: false,
    dots: false,
    arrows: false,
    slidesToShow: 3.5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2.5,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1.5,
        }
      }
    ]
  });

  $('.holiday-slider-items-wrap').slick({
    infinite: false,
    dots: false,
    arrows: true,
    slidesToShow: 3.5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2.5,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1.5,
        }
      }
    ]
  });

  // slick-slider on 'explore-section' of homepage
  $('.explore-slider-wrap').slick({
    infinite: false,
    dots: false,
    arrows: true,
    slidesToShow: 2.1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1.5,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });

  // slick slider on 'slider-items-wrap'
  $('.slider-items-wrap').slick({
    infinite: false,
    dots: false,
    arrows: true,
    slidesToShow: 4.5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3.5,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2.5,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1.5,
        }
      }
    ]

  });

  $('.single-page-scroll-menu').on('click', 'a', function (e) {

    e.preventDefault();
    var targetEl = $(this).attr('href');

    if ($(targetEl).length) {
      var targetOffsetTop = $(targetEl).offset().top;
      $('html, body').animate({
        'scrollTop': targetOffsetTop
      }, 500);
    }

  });

  const text = document.querySelector(".circle-text");
  if (text) {
    text.innerHTML = text.innerText
      .split("")
      .map(
        (char, i) => `<span style="transform:rotate(${i * 10}deg">${char}</span>`
      )
      .join("");

  }

  // custom tab
  jQuery('.tabs .tab-links a').on('click', function (e) {
    var currentAttrValue = jQuery(this).attr('href');

    // Show/Hide Tabs
    jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();

    // Change/remove current tab to active
    jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
    updateSearchLinks($('body .tab-links li.active a').attr('href'));
    e.preventDefault();
  });

  // update search links from homepage header tab that goes to specific page
  function updateSearchLinks(tabName){
    var searchLink = 'flight-search-loading.html';

    if(tabName == '#tab-hotel'){
      searchLink = 'hotel-search-listing.html';
    }
    // else if( tabName == '#tab-visa' ) {
    //   searchLink = 'check-eligibility.html';
    // }

    $("body #search-link").attr('href', searchLink);
  }

  // tab-content where there is slick-slider inside
  const itemLinks = document.querySelectorAll('.offer-section .offer-tab-links li');
  const itemDetails = document.querySelectorAll('.offer-section .tab-content .tab');

  itemLinks.forEach((link, index) => {

    link.addEventListener('click', (e) => {
      e.preventDefault();
      itemLinks.forEach(link => {
        link.classList.remove('active');
        itemLinks[index].classList.add('active');
      });

      itemDetails.forEach(itemDetail => {
        itemDetail.classList.remove('active');
        itemDetails[index].classList.add('active');
      });

    });

  });

  // reusable tab-content function
  function initializeTabs(tabLinks, tabContents) {
    const itemLinks = document.querySelectorAll(tabLinks);
    const itemDetails = document.querySelectorAll(tabContents);

    itemLinks.forEach((link, index) => {
      link.addEventListener('click', (e) => {
        if (e.target.tagName.toLowerCase() === 'a') {
          e.preventDefault();
        }
        itemLinks.forEach(link => {
          link.classList.remove('active');
        });
        itemLinks[index].classList.add('active');

        itemDetails.forEach(itemDetail => {
          itemDetail.classList.remove('active');
        });
        itemDetails[index].classList.add('active');
      });
    });
  }

  // for flight-listing page in 'fare-comparison' section
  initializeTabs('.flights-fare-tab > ul li', '.flights-fare-tab-items-wrap .flights-fare-tab-item');
  initializeTabs('.flight-inner-tab-links ul li', '.flight-inner-tab-content .flight-inner-tab-item');

  // for homepage tab-visa content
  initializeTabs('#tab-visa .select-way .form-check label', '.tab-visa-detail-wrap .tab-visa-detail-content');

  // flight-fare-detail-refund div show hide on flight-listing page
  $(".flight-fare-detail-refund a").click(function(e) {
    e.preventDefault();
    $(this).toggleClass("active");
    if ($(this).hasClass("active")) {
      $(this).text("Close Flight Details");
    }
    else {
      $(this).text("View Flight Details");
    }
    $(this).parent().parent().siblings(".flight-detail-rule-wrap").slideToggle(400)
  })

  // view-more-same-flights-div show hide on flight-listing page
  $(".view-more-same-flights").click(function() {
    $(this).toggleClass('expanded');
    $(this).siblings(".view-more-same-flights-items-wrap").slideToggle(400)
  })

  // slick slider on fare-selection popup on flight-listing page
  $('.fare-selection-slider').slick({
    infinite: false,
    dots: false,
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });

  // slick slider on 'choose-cars-slider' on booking-success-flight-cab page
  $('.airport-transfers .choose-cars-slider').slick({
    infinite: false,
    dots: false,
    arrows: true,
    slidesToShow: 3.5,
    slidesToScroll: 1,
  });

  // slick slider on add-option content in trip-customization page
  $('.add-option-slider, .hotel-room-slider').slick({
    infinite: true,
    dots: false,
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          infinite: false,
          slidesToShow: 2.5,
        }
      },
      {
        breakpoint: 768,
        settings: {
          infinite: false,
          slidesToShow: 1.5,
        }
      },
      {
        breakpoint: 376,
        settings: {
          infinite: true,
          slidesToShow: 1,
        }
      }
    ]
  });

  // view-more-hotels div show hide on trip-customization page
  var $showMoreHotels = $(".add-hotel .show-more-link a");

  $showMoreHotels.click(function(e) {
    e.preventDefault();
    $(this).toggleClass('expanded');
    if ($(this).hasClass("expanded")) {
      $(this).text("Show Less");
    }
    else {
      $(this).text("Show More");
    }
    $(this).parent().siblings(".add-option-slider-wrap").children(".slider-hidden-initially").toggleClass('slider-shown');
  })

  // view-hotels-room div show and hide on trip-customization page
  var $hotelRoomSlider = $('.add-option-content-wrap .hotel-room-wrapper');
  var $closeHotelRoomSlider = $('.add-option-content-wrap .close-room-slider');

  $(".add-hotel-content .offer-hotels-btn-wrap a").click(function (e) {
    e.preventDefault();
    // var $hotelRoomSlider = $(this).parent().parent().parent().parent().parent().parent().parent().parent().siblings(".hotel-room-wrapper");
    if ($hotelRoomSlider.height() == 0) {
      $hotelRoomSlider.animate({ height: "100%" }, 150, function () {
        $(this).addClass('shown');
        $showMoreHotels.addClass("d-none");
      });
    }
  });

  $closeHotelRoomSlider.click(function (e) {
    e.preventDefault();
    $hotelRoomSlider.animate({ height: "0" }, 150, function () {
      $(this).removeClass('shown');
      $showMoreHotels.removeClass("d-none");
    });
  });

  // show hide ride on trip-customization page
  var $addRide = $('.add-car-content .offer-cars-btn-wrap a');
  var $addRideDetail = $('.add-car .add-ride-detail-toggle');
  var $addRideDetailClose = $('.add-car .add-ride-detail-wrap .cross-icon img');

  $addRide.click(function(e) {
    e.preventDefault();
    $addRideDetail.slideDown(350);
  });

  $addRideDetailClose.click(function() {
    $addRideDetail.slideUp(350);
  });

  // e-visa-eligibility appointment form toggle on check-eligibility-page
  $(".service-assistance-msg label").on("click", function(){
    $(this).parent().parent().parent().siblings(".visa-service-assistance").slideToggle(350);
  })

  // assistance popup that comes when clicked on fixed circle content located on right side on all pages
  $(".circle-text-wrap").click(function () {
    console.log('test');
    $(".assistance-popup-content").fadeToggle(500);
  })

  /* back-to-top button*/
  $('.back-to-top').hide();
  $('.back-to-top').on("click", function (e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
  });
  $(window).scroll(function () {
    var scrollheight = 900;
    if ($(window).scrollTop() > scrollheight) {
      $('.back-to-top').fadeIn();

    } else {
      $('.back-to-top').fadeOut();
    }
  });

});

// selecting countries and currency on header
const countrySelection = document.querySelector('.country-selection-wrap');
const selectionPopup = document.querySelector('.selection-content');

if (countrySelection) {
  countrySelection.addEventListener('click', () => {
    selectionPopup.classList.toggle('d-none');
  });
}

// sign-in popup
const html = document.querySelector('html');
const login = document.querySelector('.create-acc-btn .login');
const signInPopup = document.querySelector('.sign-in-popup-wrapper');

const popupBox = document.querySelector('.popup-box');
const popupClose = document.querySelector('.popup-box .cross-icon i');

const firstSignIn = document.querySelector('.first-sign-in');
const firstSignInBtn = document.querySelector('.first-sign-in .go-next-btn');

const verifyPart = document.querySelector('.verify-part');
const passwordPart = document.querySelector('.password-part');
const emailPart = document.querySelector('.email-part');
const emailPartFromForgotPassword = document.querySelector('.forgot-password-email-part');
const verifyEmailPart = document.querySelector('.verify-email-part');
const resetPasswordPart = document.querySelector('.reset-password-part');

const goBackIcon = document.querySelector('.popup-box .go-back-icon');

const verifyPartSignInBtn = document.querySelector('.verify-part .go-next-btn');
const verifyPartPasswordSignIn = document.querySelector('.verify-part .other-option-sign-in');

const passwordPartSignInBtn = document.querySelector('.password-part .go-next-btn');
const passwordPartPasswordSignIn = document.querySelector('.password-part .other-option-sign-in');

const goBackToPasswordPart = document.querySelector('.email-part .go-back-icon img');
const goBackToPasswordPartFromEmail = document.querySelector('.forgot-password-email-part .go-back-icon img');

const emailPartSignInBtn = document.querySelector('.email-part .go-next-btn');

const forgotPassword = document.querySelector('.password-part .forgot-password');

const forgotPasswordEmailPartSignInBtn = document.querySelector('.forgot-password-email-part .go-next-btn');

const goBackToEmailPart = document.querySelector('.verify-email-part .go-back-icon img');

const verifyEmailPartSignInBtn = document.querySelector('.verify-email-part .go-next-btn');
const resetPasswordPartSignInBtn = document.querySelector('.reset-password-part .go-next-btn');

const backToLogin = document.querySelectorAll('.back-to-login a');

if (signInPopup) {
  login.addEventListener('click', (e) => {
    e.preventDefault();
    signInPopup.classList.remove('hidden');
    html.classList.add('no-scroll');
  });

  function startAgain() {
    html.classList.remove('no-scroll');
    signInPopup.classList.add('hidden');
    firstSignIn.classList.remove('d-none');
    verifyPart.classList.add('d-none');
    passwordPart.classList.add('d-none');
    emailPart.classList.add('d-none');
    emailPartFromForgotPassword.classList.add('d-none');
    resetPasswordPart.classList.add('d-none');
  }

  function startLogin() {
    firstSignIn.classList.remove('d-none');
    verifyPart.classList.add('d-none');
    passwordPart.classList.add('d-none');
    emailPart.classList.add('d-none');
    emailPartFromForgotPassword.classList.add('d-none');
    verifyEmailPart.classList.add('d-none');
    resetPasswordPart.classList.add('d-none');
  }

  popupBox.addEventListener('click', e => {

    e.preventDefault();
    e.stopPropagation();

    if (e.target.contains(popupClose)) {
      startAgain();
    }

    if (e.target === firstSignInBtn) {
      firstSignIn.classList.add('d-none');
      verifyPart.classList.remove('d-none');
    }

    if (e.target === verifyPartSignInBtn) {
      startAgain();
    }

    if (e.target === verifyPartPasswordSignIn) {
      verifyPart.classList.add('d-none');
      passwordPart.classList.remove('d-none');
    }

    if (e.target === passwordPartSignInBtn) {
      startAgain();
    }

    if (e.target === passwordPartPasswordSignIn) {
      passwordPart.classList.add('d-none');
      verifyPart.classList.remove('d-none');
    }

    if (e.target === goBackToPasswordPart) {
      emailPart.classList.add('d-none');
      passwordPart.classList.remove('d-none');
    }

    if (e.target === emailPartSignInBtn) {
      startAgain();
    }

    if (e.target === forgotPassword) {
      passwordPart.classList.add('d-none');
      emailPartFromForgotPassword.classList.remove('d-none');
    }

    if (e.target === goBackToPasswordPartFromEmail) {
      emailPartFromForgotPassword.classList.add('d-none');
      passwordPart.classList.remove('d-none');
    }

    if (e.target === forgotPasswordEmailPartSignInBtn) {
      emailPartFromForgotPassword.classList.add('d-none');
      verifyEmailPart.classList.remove('d-none');
    }

    if (e.target === goBackToEmailPart) {
      verifyEmailPart.classList.add('d-none');
      emailPartFromForgotPassword.classList.remove('d-none')
    }

    if (e.target === verifyEmailPartSignInBtn) {
      verifyEmailPart.classList.add('d-none');
      resetPasswordPart.classList.remove('d-none');
    }

    if (e.target === resetPasswordPartSignInBtn) {
      resetPasswordPart.classList.add('d-none');
      firstSignIn.classList.remove('d-none');
    }

  });

  backToLogin.forEach(login => {
    login.addEventListener('click', (e) => {
      e.preventDefault();
      startLogin();
    })
  });
}

// fare-selection-popup on flight-listing page

const openFarePopupBtn = document.querySelector('.flights-fare-tab-item-content.turkish-airline .box-button');
const farePopup = document.querySelector('.fare-selection-popup-wrap');
const farePopupClose = document.querySelector('.fare-selection-popup-content .cross-icon');

if(farePopup){
  openFarePopupBtn.addEventListener('click', () => {
    farePopup.classList.remove('hidden');
    htmlNoScroll();
  })

  farePopupClose.addEventListener('click', () => {
    farePopup.classList.add('hidden');
    htmlScroll();
  })
}

// Reusable function for popups which are initally displayed none
function popupOpenDirect(popupOpenBtn, popupWrap){
  popupOpenBtn.addEventListener('click', (e) => {
    e.preventDefault();
    popupWrap.classList.remove('d-none');
    htmlNoScroll();
  });
}

function popupCloseDirect(popupCloseBtn, popupWrap){
  popupCloseBtn.addEventListener('click', () => {
    popupWrap.classList.add('d-none');
    htmlScroll();
  });
}

function htmlNoScroll(){
  html.classList.add('no-scroll');
}

function htmlScroll(){
  html.classList.remove('no-scroll');
}

// all popup contents on tab-visa-detail on homepage
const tabVisaPopup = document.querySelector('.tab-visa-detail-wrap .popup-wrap');

function tabVisaPopupStartAgain(){
  tabVisaPopup.classList.add('d-none');
  continueRegistrationPopup.classList.add('d-none');
  applicationCheckPopup.classList.remove('d-none');
}

// application-check-popup
const applicationCheckPopup = document.querySelector('.application-check-popup-content');
const applicationCheckPopupOpen = document.querySelector('.tab-visa-application .search');
const applicationCheckYesBtn = document.querySelector('.application-check-popup-content .yes-btn');

// continue-registration-popup
const continueRegistrationPopup = document.querySelector('.continue-registration-popup-content');
const continueRegistrationPopupClose = document.querySelector('.continue-registration-popup-content .cross-icon img');
const loginExistingAccountBtn = document.querySelector('.continue-registration-popup-content .login-existing-account-btn');
const noAccountBtn = document.querySelector('.continue-registration-popup-content .no-account-btn');

// registration-via-booking-id-popup
const registrationViaBookingPopup = document.querySelector('.registration-via-booking-id-popup-content');

if(tabVisaPopup){
  popupOpenDirect(applicationCheckPopupOpen, tabVisaPopup);

  tabVisaPopup.addEventListener('click', (e)=> {

    if(e.target === applicationCheckYesBtn){
      applicationCheckPopup.classList.add('d-none');
      continueRegistrationPopup.classList.remove('d-none');
    }

    if(e.target === continueRegistrationPopupClose){
      tabVisaPopupStartAgain();
    }

    if(e.target === loginExistingAccountBtn){
      tabVisaPopupStartAgain();
      // sign-in-popup that is in header part
      signInPopup.classList.remove('hidden');
    }

    if(e.target === noAccountBtn){
      continueRegistrationPopup.classList.add('d-none');
      registrationViaBookingPopup.classList.remove('d-none');
    }

  })
}

// book-appointment popup on book-consultation page
const bookAppointmentPopup = document.querySelector('.book-appointment-popup-wrap');
const bookAppointmentPopupOpen = document.querySelector('.book-consultation-item .book-consultation-btn');
const bookAppointmentPopupClose = document.querySelector('.book-appointment-popup-content .cross-icon');

if(bookAppointmentPopup){
  popupOpenDirect(bookAppointmentPopupOpen, bookAppointmentPopup);
  popupCloseDirect(bookAppointmentPopupClose, bookAppointmentPopup); 
}

// reschedule-appointment popup on book-consultation page
const rescheduleAppointmentPopup = document.querySelector('.reschedule-appointment-popup-wrap');
const rescheduleAppointmentPopupOpen = document.querySelector('.book-consultation-item .reschedule-consultation-btn');
const rescheduleAppointmentPopupClose = document.querySelector('.reschedule-appointment-popup-content .cross-icon');

if(rescheduleAppointmentPopup){
  popupOpenDirect(rescheduleAppointmentPopupOpen, rescheduleAppointmentPopup);
  popupCloseDirect(rescheduleAppointmentPopupClose, rescheduleAppointmentPopup); 
}

// calendar-popup on flight-listing page
const fareCalendarPopup = document.querySelector('.fare-calendar-popup-wrap');
const openFareCalendarPopup = document.querySelector('.fare-calendar');
const fareCalendarPopupClose = document.querySelector('.fare-calendar-popup-content .cross-icon');

if(fareCalendarPopup){
  popupOpenDirect(openFareCalendarPopup, fareCalendarPopup);
  popupCloseDirect(fareCalendarPopupClose, fareCalendarPopup); 
}


// Child Seat
const childSeatPopup = document.querySelector('.child-seat-request-wrap');
const openChildSeatPopup = document.querySelector('.child-seat-request');
const childSeatPopupClose = document.querySelector('.child-seat-popup-content .cross-icon');
const childSeatPopupButtonClose = document.querySelector('.child-seat-popup-content #child-seat-btn');

if(childSeatPopup){
  popupOpenDirect(openChildSeatPopup, childSeatPopup);
  popupCloseDirect(childSeatPopupClose, childSeatPopup); 
  popupCloseDirect(childSeatPopupButtonClose, childSeatPopup);
}

// hotel-secured-msg popup on hotel-search-single-page
const hotelSecuredPopup = document.querySelector('.hotel-secured-popup-wrap');
const openhotelSecuredPopup = document.querySelector('.hotel-apartment-price-summary .box-button');
const hotelSecuredPopupClose = document.querySelector('.hotel-secured-popup-content .cross-icon');

if(hotelSecuredPopup){
  popupOpenDirect(openhotelSecuredPopup, hotelSecuredPopup);
  popupCloseDirect(hotelSecuredPopupClose, hotelSecuredPopup); 
}

// guest-review popup on hotel-search-single-page
const guestReviewPopup = document.querySelector('.hotel-guest-reviews-popup-wrap');
const openGuestReviewPopupBtn = document.querySelector('.single-page-hotel-content-wrap .featured-image-review-wrap');
const guesReviewPopupClose = document.querySelector('.hotel-guest-reviews-popup-wrap .cross-icon');

if(guestReviewPopup){
  openGuestReviewPopupBtn.addEventListener('click', () => {
    guestReviewPopup.classList.add('review-shown');
    htmlNoScroll();
  })

  guesReviewPopupClose.addEventListener('click', () => {
    guestReviewPopup.classList.remove('review-shown');
    htmlScroll();
  })
}

// e-visa-eligibility popup on check-eligibility-page
const eVisaEligibilityPopup = document.querySelector('.e-visa-eligibility-popup-wrap');
const checkEligibilityPopupBtn = document.querySelector('.check-eligibility-btn');
const eVisaEligibilityPopupCloseBtn = document.querySelector('.e-visa-eligibility-popup-wrap .box-button');
const visaRequirement = document.querySelector('.check-eligibility-section .visa-requirement');
const checkEligibilityBtn = document.querySelector('.check-eligibility-section .check-eligibility-btn');
const btnsWrap = document.querySelector('.check-eligibility-section .back-and-continue-btns-wrap');

if(eVisaEligibilityPopup){
  checkEligibilityPopupBtn.addEventListener('click', () => {
    eVisaEligibilityPopup.classList.add('popup-shown');
    htmlNoScroll();
  })

  eVisaEligibilityPopupCloseBtn.addEventListener('click', () => {
    eVisaEligibilityPopup.classList.remove('popup-shown');
    checkEligibilityBtn.classList.add('d-none');
    visaRequirement.classList.remove('d-none');
    btnsWrap.classList.remove('d-none');
    htmlScroll();
  })
}

// edit-information popup on review-and-submit-page
const editInformationPopup = document.querySelector('.edit-information-popup-wrap');
const editInformationPopupOpen = document.querySelector('.review-your-information-section .personal-information .edit-btn');
const editInformationPopupClose = document.querySelector('.edit-information-popup-wrap .cross-icon');
const updateInformationBtn = document.querySelector('.edit-information-popup-wrap .box-button');

if(editInformationPopup){
  popupOpenDirect(editInformationPopupOpen, editInformationPopup);
  popupCloseDirect(editInformationPopupClose, editInformationPopup);
  popupCloseDirect(updateInformationBtn, editInformationPopup); 
}

// gallery image change on things-to-do-single-page
const galleryImages = document.querySelector('.page-title-image-gallery');

if(galleryImages){
  let slideIndex = 1;
  showSlides(slideIndex);
  
  function currentSlide(n) {
    showSlides(slideIndex = n);
  }
  
  function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("big-img-slide");
    let slidingImages = document.getElementsByClassName("sliding-img");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    for (i = 0; i < slidingImages.length; i++) {
      slidingImages[i].classList.remove("active");
    }
    slides[slideIndex - 1].style.display = "block";
    slidingImages[slideIndex - 1].classList.add("active");
  } 
}