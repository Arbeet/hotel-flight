/*  12.0 ===== Medias/Responsive =====   */

/*
 * Does the same thing as <meta name="viewport" content="width=device-width">,
 * but in the future W3C standard way. -ms- prefix is required for IE10+ to
 * render responsive styling in Windows 8 "snapped" views; IE10+ does not honor
 * the meta tag. See https://core.trac.wordpress.org/ticket/25888.
 */

@-ms-viewport {
  width: device-width;
}

@viewport {
  width: device-width;
}

@media only screen and (max-width: 1299px) {

.flight-round-btn-wrap .box-button, 
.offer-hotels-btn-wrap .box-button, 
.offer-cars-btn-wrap .box-button {
  padding: 6px 12px 5px;
}
.flight-round-card-payment a, 
.offer-hotels-card-payment a {
  font-size: 12px;
}
.info h5 {
  font-size: 18px;
}
.flight-fare-payment-wrap {
  gap: 20px;
}


}


/* All Tablet Portrait size smaller than standard 1199 (devices and browsers) */

@media only screen and (max-width: 1199px) {
  .site-branding {
    width: 180px;
  }
  .hgroup-wrap .hgroup-right {
    width: 81%;
    width: calc(100% - 180px);
    width: -webkit-calc(100% - 180px);
    padding-left: 30px;
    gap: 30px;
  }
  .hgroup-right .navbar {
    width: 61%;
    width: calc(100% - 290px);
    width: -webkit-calc(100% - 290px);
  }
  .right-header-content {
    width: 290px;
    gap: 20px;
  }
  .menu-top-menu-container > ul {
    gap: 6px;
  }
  .main-navigation ul li a {
    padding: 10px 8px;
  }
  .consultation-btn-wrap .box-button {
    font-size: 15px;
    padding: 16px 20px 15px;
  }
  .book-consultation-item {
    padding: 40px 15px;
  }
  .site-content section.book-consultation {
    padding-bottom: 80px;
  }
  .tab-detail .info h3 {
    font-size: 22px;
  }
  .header-tab-section .search {
    font-size: 22px;
  }
  .site-main > section {
    padding-right: 0;
    padding-left: 0;
  }
  .vertical-text {
    padding-right: 3px;
    box-shadow: 3px -2px 5px 3px rgba(0,0,0,0.15);
  }
  .offer-flight-item .featured-image {
    width: 150px;
  }
  .offer-flight-item .offer-flight-content-wrap {
    width: 55%;
    width: calc(100% - 150px);
    width: -webkit-calc(100% - 150px);
    padding-left: 10px;
    position: relative;
  }
  .offer-flight-content-wrap h3 {
    font-size: 18px;
  }
  .round-title-detail {
    gap: 10px;
    margin-top: 12px;
  }
  .offer-airline-logo-wrap {
    top: -6px;
  }
  .offer-flight-content-wrap h3 {
    font-size: 17px;
  }
  .offer-flight-item,
  .offer-hotels-item, 
  .offer-cars-item  {
    padding: 12px;
  }
  .entry-header.heading .entry-title {
    font-size: 32px;
  }
  .offer-hotels-title-wrap h3, .offer-cars-title-wrap h3 {
    font-size: 18px;
  }
  .tab-detail .info h3 span {
    font-size: 19px;
  }
  .wakanow-app-content-wrap {
    padding: 30px;
  }
  .handpicked-collections .entry-title,
  .lesser-known-wonders .entry-title {
    font-size: 15px;
  }
  .slider-content-wrap h2.entry-title {
    font-size: 28px;
  }
  .wakapoint-text-content {
    padding-right: 30px;
  }
  .wakapoint-content-section, .wakapoint-great-deal-content-section {
    padding: 30px 25px;
  }
  .info h5 {
    font-size: 15px;
  }
  .flights-filter-result .heading-wrap .entry-title {
    font-size: 18px;
  }
  .flight-fare-detail-header-wrap {
    grid-template-columns: calc(40% - 10px) calc(60% - 10px);
  }
  .flight-timing h5 {
    font-size: 18px;
  }
  .flights-fare-tab-items-wrap .flight-fare-detail-wrap {
    padding: 20px 15px;
  }
  .user-travel-detail .info {
    padding: 10px 10px;
  }
  .airport-transfer-heading-wrap {
    padding: 30px 25px 0 25px;
  }
  .airport-transfer-heading-wrap h3 {
    font-size: 26px;
  }
  .fare-selection-slider .slick-next {
    right: -7px;
  }
  .fare-selection-slider .slick-prev {
    left: -7px;
  }
  .page-title-link-wrap .entry-title {
    font-size: 20px;
    margin-top: 4px;
  }
  .flight-process-page-title .flight-process-active-info {
    width: 480px;
  }
  .flight-process-page-title .page-title-link-wrap {
    width: 57%;
    width: calc(100% - 480px);
    width: -webkit-calc(100% - 480px);
    padding-right: 10px;
  }
  .flight-process-active-info ul {
    gap: 10px;
  }
  .flight-process-active-info ul li {
    font-size: 11px;
  }
  .flight-search-waka-points h4 span {
    font-size: 18px;
  }
  .flight-search-waka-points .waka-gift-icons {
    max-width: 33px;
  }
  .transport-info li h6 {
    font-size: 13px;
  }
  .trip-total .price {
    font-size: 18px;
  }
  .trip-total .total-heading {
    font-size: 16px;
  }
  .promo-code h6 {
    font-size: 14px;
  }
  .flight-search-time-content, .flight-search-change-planes-content {
    font-size: 14px;
  }



}

/* All Tablet Portrait size smaller than standard 1023 (devices and browsers) */

@media only screen and (max-width: 1023px) {
  
  .hgroup-wrap .navbar {
    position: static;
  }
  .hgroup-wrap .hgroup-right {
    padding-right: 55px;
  }
  .mean-container .meanmenu-reveal {
    right: -10px !important;
  }
  .mean-container .meanmenu-reveal span,
  .mean-container .meanmenu-reveal span::before, 
  .mean-container .meanmenu-reveal span::after {
    background: var(--bg);
  }
  .mean-container .mean-nav > ul {
    z-index: 999;
    background: var(--secondary);
    left: 0;
    top: 60px;
  }
  .main-navigation ul li a .icon-wrap {
    margin: 0 10px 0 0;
  }
  .mean-container .mean-nav ul li a {
    padding: 12px 3%;
    display: flex;
  }
  .mean-container .mean-nav ul li a:hover {
    background: var(--white);
    color: var(--primary);
  }
  .mean-container .meanmenu-reveal span {
    width: 30px;
  }
  .offer-flight-item .featured-image {
    width: 100%;
  }
  .offer-flight-item .offer-flight-content-wrap {
    padding-left: 0;
    width: 100%;
    margin-top: 10px;
  }
  .offer-flight-item .featured-image img {
    width: 100%;
  }
  .offer-airline-logo-wrap {
    top: 0px;
  }


}

@media only screen and (max-width: 991px) {
  .entry-title {
    font-size: 24px;
  }
  .page-title-link-wrap .entry-title {
    margin-top: 4px;
  }
  .book-consultation-item {
    padding: 40px 20px;
    width: 47%;
    width: calc(50% - 15px);
    width: -webkit-calc(50% - 15px);
  }
  .site-footer .widget-area div[class*="custom-"],
  .site-footer .widget-area div[class*="custom-"]:first-child,
  .site-footer .widget-area div[class*="custom-"]:last-child {
    width: 50%;
  }
  .site-footer .widget-area div[class*="custom-"]:nth-child(2n+1) {
    clear: both;
  }
  .tab-detail .info {
    padding: 10px;
  }
  .tab-detail .info h3 {
    font-size: 20px;
  }
  .tab-detail .info.go-to {
    padding-left: 10px;
  }
  .tab-detail .info h3 span {
    font-size: 17px;
  }
  .tab-detail .info .info-heading {
    font-size: 13px;
  }
  .header-tab-section .search {
    font-size: 20px;
    padding: 8px 45px;
  }
  .waka-gift-content h4 {
    font-size: 13px;
  }
  .offer-section-content {
    padding: 15px;
  }
  .offer-section-content .entry-header.heading {
    top: 20px;
    left: 15px;
  }
  .entry-header.heading .entry-title {
    font-size: 28px;
  }
  .waka-info-detail-item {
    width: 100%;
    padding: 12px;
  }
  .waka-info-detail-item-wrap {
    gap: 8px;
  }
  .wakanow-app-content-wrap {
    padding: 25px 15px;
  }
  .wakanow-get-app-input {
    width: 33%;
  }
  .wakanow-app-scan-section {
    padding-left: 15px;
  }
  .wakanow-get-app-form-wrap h2 {
    font-size: 28px;
  }
  .handpicked-collections .slider-content-wrap,
  .lesser-known-wonders .slider-content-wrap {
    padding: 30px 15px;
  }
  .wakapoint-text-content {
    width: 100%;
    padding-right: 0;
  }
  .wakapoint-content-section, 
  .wakapoint-great-deal-content-section {
    display: block;
  }
  .wakapoint-content-image-section, 
  .wakapoint-great-deal-image-section {
    width: 420px;
  }
  .wakapoint-content-section, 
  .wakapoint-great-deal-content-section {
    width: 43%;
    width: calc(100% - 420px);
    width: -webkit-calc(100% - 511px);
  }
  .wakapoint-text-content h2 {
    font-size: 18px;
  }
  h2 {
    font-size: 26px;
  }
  .wakapoint-home-packages-content-wrap h2 {
    font-size: 18px;
  }
  .wakapoint-home-packages-content-wrap h4 {
    font-size: 17px;
  }
  .stay-in-info-wrap {
    padding: 40px 0 20px;
    display: block;
  }
  .stay-in-info-item h4.entry-title {
    font-size: 17px;
    margin-bottom: 10px;
  }
  .site-main > section.stay-in-info {
    padding-top: 25px;
  }
  .wakapoint-btn-wrap .featured-image {
    margin-bottom: 18px;
    margin-top: 15px;
  }
  .round-title-detail {
    margin-top: 5px;
  }
  .flight-listing-title-section .user-travel-detail .info-wrap,
  .flight-loading-title-section .user-travel-detail .info-wrap {
    display: flex;
    flex-wrap: wrap;
  }
  .flight-listing-title-section .user-travel-detail .info.go-to,
  .flight-loading-title-section .user-travel-detail .info.go-to {
    margin-left: 0;
  }
  .flight-listing-title-section .user-travel-detail .info.from ,
  .flight-loading-title-section .user-travel-detail .info.from {
    margin-right: 0;
  }
  .user-travel-detail .info {
    width: 49%;
    width: calc(50% - 3px);
    width: -webkit-calc(50% - 3px);
  }
  .flight-listing-title-section .user-travel-detail .info.go-to::before,
  .flight-loading-title-section .user-travel-detail .info.go-to::before {
    display: none;
  }
  .flights-filter-content, .hotel-search-filter-content {
    display: flex;
    flex-wrap: wrap;
    gap: 0;
  }
  .filter-selection {
    -webkit-box-ordinal-group: 3;
        -ms-flex-order: 2;
            order: 2;
    width: 100%;
  }
  .flights-filter-result {
    -webkit-box-ordinal-group: 2;
        -ms-flex-order: 1;
            order: 1;
    width: 100%;
  }
  .site-content section.page-title-section {
    padding: 40px 0 160px;
  }
  .filter-selection .airport-transfer-advertisement {
    text-align: center;
  }
  .fare-selection-payment h4 {
    font-size: 18px;
  }
  .fare-selection-payment span {
    font-size: 12px;
  }
  .fare-selection-payment.pay-small label h4 {
    font-size: 20px;
  }
  .fare-selection-payment {
    padding: 7px 15px;
  }
  .fare-selection-payment-wrap .box-button {
    padding: 13px 60px;
  }
  .flight-search-page-section .custom-col-9 ,
  .flight-search-page-section .custom-col-3 {
    width: 100%;
  }
  .flight-search-page-section .custom-col-9 {
    margin-bottom: 20px;
  }


}

/* All Mobile Portrait size smaller than 768 (devices and browsers) */

@media only screen and (max-width: 767px) {
  .custom-col-1, .custom-col-2, .custom-col-3, .custom-col-4, .custom-col-5, .custom-col-6, .custom-col-7, .custom-col-8, .custom-col-9, .custom-col-10, .custom-col-11, .custom-col-12 {
    float: none;
    width: 100%;
  }
  .book-consultation-item-wrapper {
    gap: 15px;
    padding-right: 0;
    width: 100%;
  }
  .circle-text-wrapper {
margin-top: 30px;
    position: fixed;
    right: 7px;
    top: 35%;
    bottom: 11px;
    z-index: 9999;
    height: 110px;
    width: 110px;
  }

  .site-content section.book-consultation {
    padding-bottom: 40px;
  }
  .site-footer .widget-area div[class*="custom-"], .site-footer .widget-area div[class*="custom-"]:first-child, .site-footer .widget-area div[class*="custom-"]:last-child {
    width: 100%;
  }
  .hgroup-wrap .hgroup-right {
    padding-right: 50px;
    padding-left: 5px;
    gap: 0;
    width: 64%;
    width: calc(100% - 160px);
    width: -webkit-calc(100% - 160px);
  }
  .create-acc-btn a {
    font-size: 13px;
  }
  .hgroup-wrap .navbar {
    width: 0;
  }
  .site-branding {
    width: 160px;
  }
  .bg-siteheader .hgroup-wrap .hgroup-right {
    display: none;
  }
  .header-tab-section .tab-links li {
    margin-bottom: 10px;
  }
  .header-tab-section .tabs .tab-content {
    padding: 25px 20px 40px 20px;
    margin-top: 10px;
  }
  .header-tab-section .select-trip-class {
    width: 100%;
  }
  .header-tab-section .choose-method .select-wrap {
    display: block;
    width: 290px;
    margin: 0 auto;
  }
  .header-tab-section .select-way {
    margin-bottom: 10px;
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
  }
  .header-tab-section .select-trip-class select {
    padding: 11px 24px;
    border-radius: 5px;
  }
  .tab-detail.flight-detail {
    display: flex;
    flex-wrap: wrap;
    gap: 10px;
    border: none;
    border-top: 1px dashed var(--gray);
    border-radius: 0;
    padding-top: 15px;
    margin-top: 15px;
  }
  .tab-detail.flight-detail > div {
    width: 100%;
    border: 1px solid var(--gray);
    border-radius: 8px;
    min-height: 80px;
  }
  .tab-detail.flight-detail > div.departure, .tab-detail.flight-detail > div.return {
    width: 49%;
    width: calc(50% - 5px);
    width: -webkit-calc(50% - 5px);
  }
  .tab-detail .info .info-heading {
    font-size: 10px;
    margin-bottom: 5px;
  }
  .tab-detail .info {
    padding: 5px 10px;
  }
  .tab-detail.flight-detail > div span {
    font-size: 12px;
  }
  .waka-gift-item {
    padding: 10px 15px;
    width: 100%;
  }
  .waka-gift-item-wrap {
    padding: 5px 0;
  }
  .waka-gift-item-wrap {
    margin-top: -90px;
  }
  .bg-siteheader {
    padding-bottom: 150px;
  }
  .offer-section-content {
    background: none;
    border-radius: 0;
    padding: 0;
  }
  .site-main > section ,
  .site-content section.waka-gift-section{
    padding: 0 0 30px;
  }
  .offer-section-content .entry-header.heading {
    position: static;
    text-align: left;
    padding-bottom: 10px;
  }
  .offer-section-content .offer-tab-links {
    margin-left: 0;
    width: 100%;
    justify-content: flex-start;
  }
  .wakanow-app-content-wrap {
    display: block;
  }
  .wakanow-app-content {
    width: 100%;
    margin-bottom: 20px;
  }
  .wakanow-app-scan-section {
    padding-left: 0;
    width: 100%;
  }
  .fare-calendar-wrap .fare-calendar-info {
    display: none;
  }
  .fare-calendar-wrap {
    width: 160px;
  }
  .flights-filter-result .heading-wrap .entry-title {
    width: 70%;
    width: calc(100% - 160px);
    width: -webkit-calc(100% - 160px);
  }
  .flights-filter-result .heading-wrap {
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
  }
  .flights-filter-result .heading-wrap .entry-title {
    font-size: 18px;
  }
  .site-content section.flights-filter-section {
    padding-top: 30px;
  }
  .flight-search-trip-fare-summary, .flight-search-trip-traveller-details, .flight-search-choose-payment-wrap, .flight-search-choose-seat-wrap, .flight-search-insurance-info-wrap, .add-option-content-wrap, .flight-search-customization-general-info-wrap, .flights-sorted-info-wrap, .hotel-detail-search-trip {
    padding: 10px;
  }
  .flights-sorted-important-notice p {
    font-size: 12px;
  }
  .flights-filter-airways-slider-info-wrap {
    display: none;
  }
  .flight-fare-detail-refund a {
    display: none;
  }
  .flights-fare-tab-items-wrap .flight-fare-detail-wrap {
    position: relative;
  }
  .flight-fare-payment-wrap .box-button {
    position: absolute;
    bottom: 5px;
    right: 15px;
  }
  .flight-travel-detail {
    margin-bottom: 20px;
  }
  .flight-fare-payment-wrap .full-pay {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
  }
  .flight-fare-payment-wrap .full-pay h5 {
    font-size: 14px;
  }
  .flight-fare-payment-wrap .pay-option {
    font-size: 8px;
    margin-right: 4px;
  }
  .flight-fare-payment-wrap .small-pay .pay-option {
    display: none;
  }
  .flight-fare-payment-wrap .small-pay h6 {
    font-size: 12px;
  }
  .small-amount-pay-wrap .small-pay-icon {
    width: 20px;
  }
  .flight-fare-detail-header-wrap {
    gap: 10px;
    padding-bottom: 10px;
    margin-bottom: 15px;
  }
  .flight-departure.flight-travel,
  .flight-return.flight-travel {
    border: 1px solid #B5B5B5;
    border-radius: 3px;
    padding: 13px;
  }
  .flight-travel-detail {
    grid-template-columns: 100%;
  }
  .fare-selection-content {
    grid-template-columns: 100%;
  }
  .fare-selection-popup-content {
    padding: 15px 15px;
    height: 100%;
  }
  .fare-selection-popup-content .entry-title {
    padding: 20px 15px;
    margin: -20px -15px 15px -15px;
    font-size: 18px;
  }
  .fare-selection-content .fare-details h5 {
    margin-bottom: 20px;
    font-size: 16px;
  }
  .depart-return-wrap {
    display: -ms-grid;
    display: grid;
    -ms-grid-columns: calc(50% - 5px) 10px calc(50% - 5px);
    grid-template-columns: calc(50% - 5px) calc(50% - 5px);
    gap: 10px;
  }
  .depart-return-wrap .travel-date {
    border: 1px solid #B9B9B9;
    padding: 15px;
    border-radius: 20px;
  }
  .fare-selection-content .travel-date h6 {
    font-size: 12px;
    font-weight: 400;
  }
  .fare-selection-content .travel-date span {
    font-size: 11px;
  }
  .fare-selection-payment-wrap {
    margin: 15px -15px -20px -15px;
    padding: 15px 15px;
    background: var(--bg);
    border-radius: 15px 15px 0 0;
  }
  .fare-selection-payment h4 {
    font-size: 14px;
    color: #fff;
    margin-bottom: 0;
  }
  .fare-selection-payment span {
    font-size: 10px;
    color: #fff;
  }
  .fare-selection-payment.pay-small {
    border: 1px solid #475F68;
    background: none;
  }
  .fare-selection-payment.pay-small label h4 {
    font-size: 14px;
  }
  .fare-selection-payment {
    padding: 2px 10px;
    line-height: 1;
    border: 1px solid #475F68;
    gap: 7px;
  }
  .fare-selection-payment-wrap .box-button {
    padding: 13px 33px;
    font-size: 14px;
  }
  .fare-selection-payment.pay-small label img {
    max-width: 22px;
  }
  .fare-selection-popup-content .cross-icon {
    right: 13px;
    top: 17px;
  }
 


}

@media only screen and (max-width: 567px) {
  
  .site-content section.page-title-section {
    padding: 40px 0 160px;
  }
  .entry-title {
    font-size: 22px;
  }
  .book-consultation-item {
    width: 100%;
  }
  .create-acc-btn a {
    font-size: 13px;
    font-size: 0;
    width: 25px;
    height: 25px;
    display: block;
    background: url("../images/login.png");
    background-repeat: no-repeat ;
    background-size: 20px;
  }
  .right-header-content {
    align-items: center;
  }
  .header-select-option h5 {
    font-size: 12px;
    margin-bottom: 1px;
  }
  .right-header-content > div {
    min-width: 50px;
  }
  .right-header-content {
    gap: 10px;
  }
  .create-acc-btn a {
    margin-left: auto;
  }

  .feedback-sticky,
  .right-header-content{
    display: none;
  }
  .flights-filter-result .heading-wrap .entry-title {
    font-size: 14px;
  }
  .flights-sorted-info-wrap h6 {
    padding-bottom: 10px;
    margin-bottom: 13px;
    font-size: 12px;
  }
  .flights-sorted-important-notice p {
    font-size: 10px;
  }
  .flights-sorted-important-notice {
    padding: 10px;
    margin-bottom: 5px;
  }
  .airline-heading h6 {
    font-size: 12px;
  }
  .airline-icon {
    width: 18px;
    height: 18px;
  }
  .airline-heading h6 {
    width: 92%;
    width: calc(100% - 18px);
    width: -webkit-calc(100% - 18px);
    padding-left: 5px;
  }
  .flights-fare-tab > ul li h6 {
    font-size: 12px;
    margin-bottom: 2px;
  }
  .flights-fare-tab > ul li {
    font-size: 10px;
  }
  .flights-fare-tab > ul li {
    padding: 10px;
  }
  .flight-travel-heading h6 {
    font-size: 14px;
  }
  .flight-travel-heading li {
    padding: 0 3px;
    font-size: 10px;
  }
  .flight-timing-wrap {
    padding: 13px 13px;
    gap: 10px;
  }
  .flight-timing h5 {
    font-size: 20px;
  }
  .flight-on-air span {
    padding: 5px 0;
    font-size: 13px;
  }
  .flight-fare-detail-refund .refundable {
    font-size: 12px;
  }
  .flight-fare-payment-wrap .box-button {
    font-size: 14px;
    padding: 9px 16px;
    bottom: 7px;
  }
  .view-more-same-flights span {
    font-size: 10px;
    letter-spacing: 1.9px;
  }
  .flight-search-trip-fare-summary, .flight-search-trip-traveller-details, .flight-search-choose-payment-wrap, .flight-search-choose-seat-wrap, .flight-search-insurance-info-wrap, .add-option-content-wrap, .flight-search-customization-general-info-wrap, .flights-sorted-info-wrap, .hotel-detail-search-trip {
    margin-bottom: 12px;
  }
  .flights-fare-tab > ul,
  .flights-fare-tab-item-content {
    margin: 0 0 12px 0;
  }
  .price-watch-notification p {
    font-size: 10px;
  }
  .price-watch-notification h5 {
    font-size: 12px;
  }
  .notification-bell-icon {
    width: 20px;
  }
  .price-watch-notification {
    padding: 12px 15px;
    gap: 10px;
    margin-bottom: 12px;
    border-radius: 8px;
  }
  .price-watch-heading-wrap {
    gap: 10px;
  }
  .site-content section.page-title-section.flight-loading-title-section {
    padding: 40px 0;
  }

}

/* All Mobile size smaller than standard 479 (devices and browsers) */

@media only screen and (max-width: 479px) {
 
  .site-branding {
    width: 120px;
  }
  .hgroup-wrap .hgroup-right {
    padding-right: 40px;
    padding-left: 5px;
    width: 65%;
    width: calc(100% - 120px);
    width: -webkit-calc(100% - 120px);
  }
  .page-title-link-wrap {
    display: block;
  }
  .page-title-link-wrap .entry-title {
    margin-top: 20px;
    margin-bottom: 15px;
  }
  .book-consultation-item .author-name {
    font-size: 20px;
  }
  .site-footer .widget-title {
    font-size: 18px;
  }
  .inline-social-icon li a {
    height: 38px;
    width: 38px;
    line-height: 36px;
  }
  .site-footer .inline-social-icon li {
    margin: 0 10px 7px 0 !important;
  }
  .widget > ul > li > a {
    font-size: 14px;
  }
  .site-footer .widget ul li {
    margin-bottom: 12px;
  }
  .site-generator .copy-right {
    padding: 20px 0;
  }
  .site-footer .widget {
    margin-bottom: 30px;
  }
  .widget-area {
    padding: 20px 0 0;
  }
  .mean-container .mean-nav > ul {
    top: 55px;
  }
  .back-to-top {
    right: 15px;
  }
  .user-travel-detail .info {
    width: 100%;
  }
  .flight-listing-title-section .user-travel-detail .info-wrap,
  .flight-loading-title-section .user-travel-detail .info-wrap {
    width: 100%;
  }
  .user-travel-detail .search-btn {
    width: 100%;
  }
  .user-travel-detail {
    -ms-flex-wrap: wrap;
        flex-wrap: wrap;
  }
  .site-content section.page-title-section {
    padding: 30px 0 160px;
  }
  .flights-filter-result .heading-wrap {
    display: block;
  }
  .flights-filter-result .heading-wrap .entry-title {
    width: 100%;
    padding: 0;
    margin-bottom: 15px;
  }
  .fare-calendar-wrap {
    width: 100%;
    text-align: left;
  }
  .fare-calendar-wrap .fare-calendar {
    padding: 9px 10px 8px 30px;
    font-size: 12px;
    background-size: 14px;
  }
  .flights-fare-tab-items-wrap .flight-fare-detail-wrap {
    padding: 20px 10px;
  }
  .flight-fare-payment-wrap .full-pay h5 {
    font-size: 12px;
  }
  .flight-fare-payment-wrap .small-pay h6 {
    font-size: 11px;
  }
  .flight-fare-payment-wrap {
    gap: 8px;
  }
  .flight-timing h5 {
    font-size: 16px;
  }
  .flight-start.flight-timing span {
    font-size: 12px;
  }
  .filter > h6 {
    font-size: 14px;
  }
  .airport-transfer-heading-wrap h3 {
    font-size: 22px;
  }
  h6 {
    font-size: 14px;
  }
  .site-content section.page-title-section.flight-loading-title-section {
    padding: 30px 0;
  }
  .depart-return-wrap {
    -ms-grid-columns: 100%;
    grid-template-columns: 100%;
    gap: 10px;
  }
  .depart-return-wrap .travel-date {
    margin-bottom: 0;
  }
  .fare-selection-payment-wrap {
    flex-wrap: wrap;
  }
  .fare-selection-payment-wrap .box-button {
    width: 100%;
  }
  .fare-selection-payment-content {
    justify-content: space-between;
    width: 100%;
  }
  .fare-selection-popup-content .entry-title {
    font-size: 15px;
  }
  


}

@media only screen and (max-width: 375px) {
  .flight-fare-detail-header-wrap {
    grid-template-columns: 100%;
  }
  .right-header-content > div {
    min-width: 35px;
  }
  .site-branding {
    width: 90px;
  }
  .hgroup-wrap .hgroup-right {
    padding-right: 33px;
    width: 65%;
    width: calc(100% - 90px);
    width: -webkit-calc(100% - 90px);
  }
  .mean-container .meanmenu-reveal span {
    width: 27px;
  }
  .mean-container .meanmenu-reveal {
    right: -12px !important;
  }
  .header-select-option select {
    font-size: 12px;
  }
  .mean-container .mean-nav > ul {
    top: 53px;
  }
  .site-generator .copy-right {
    font-size: 13px;
  }

}